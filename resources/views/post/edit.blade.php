@extends('layouts.admin-master')

@section('title')
Dashboard
@endsection

@section('content')


<section class="section">
  <div class="section-header">
    <h1>Create Posts</h1>
  </div>
  <div class="section-body">



                    {{ Form::model($post,['url'=>'/post/'.$post->id.'/edit','method'=>'PUT'])}}

                        @csrf

                        <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Title</label>
                                <div class="col-md-8">
                                    {{ Form::text('title',null,['class'=>'form-control'])}}
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Body</label>
                                <div class="col-xl-8">
                                    {{ Form::text('body',null,['class'=>'form-control'])}}
                                </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">

                                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                                <a href="/home" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </form>
            </div>


          </section>
          @endsection
