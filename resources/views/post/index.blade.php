@extends('layouts.datatables')

@section('title')
Manage Users
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Manage Posts </h1>


  </div>
  <a href="/post/create" class="btn btn-primary">Input Data Baru</a><br><br>

  <div class="section-body">

    <table class="table table-bordered" id="users-table">
      <thead>
          <tr>
              <th width="150">Title</th>
              <th>Body</th>
              <th width="150">Tanggal</th>
              <th width="120">Action</th>
          </tr>
      </thead>
  </table>
  </div>
</section>
@endsection
@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/post/json',
        columns: [
            { data: 'title', name: 'title' },
            { data: 'body', name: 'body' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@endpush
