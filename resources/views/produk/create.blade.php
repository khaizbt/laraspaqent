@extends('layouts.admin-master')

@section('title')
Dashboard
@endsection

@section('content')


<section class="section">
  <div class="section-header">
    <h1>Create Posts</h1>
  </div>
  <div class="section-body">
    {{ Form::open(['url'=>'produk', 'enctype' => 'multipart/form-data'])}}

        @csrf

        <div class="form-group row">
                <label class="col-md-2 col-form-label text-md-right">Title</label>
                <div class="col-md-8">
                    {{ Form::text('nama_produk',null,['class'=>'form-control'])}}
                </div>
        </div>
        <div class="form-group row">
                <label class="col-md-2 col-form-label text-md-right">Body</label>
                <div class="col-md-8">
                    {{ Form::text('deskripsi_produk',null,['class'=>'form-control'])}}
                </div>
        </div>

        <div class="form-group row">
                <label class="col-md-2 col-form-label text-md-right">Body</label>
                <div class="col-md-8">
                    {{ Form::text('stock',null,['class'=>'form-control'])}}
                </div>
        </div>

        <div class="form-group row">
                <label class="col-md-2 col-form-label text-md-right">Body</label>
                <div class="col-md-8">
                    {{ Form::text('harga',null,['class'=>'form-control'])}}
                </div>
        </div>

        <div class="form-group row">
                <label class="col-md-2 col-form-label text-md-right">Body</label>
                <div class="col-md-8">

                                  <span class="btn btn-info btn-file">
                                    <input type="file" name="photo" id="photo"> </span>

                </div>
        </div>



        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-2">

                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                <a href="/home" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </form>
  </div>
</section>
@endsection
