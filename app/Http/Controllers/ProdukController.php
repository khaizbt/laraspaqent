<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function data(){
        //
    }

    public function index()
    {
        return view('produk.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $produk = new Produk;
      $produk->nama_produk = $request->nama_produk;
      $produk->deskripsi_produk = $request->deskripsi_produk;
      $produk->stock = $request->stock;
      $produk->harga = $request->harga;
      if ($request->hasFile('photo')) {
          $produk->photo = $request->photo->hashName();
          $request->photo->store('uploads/gambar_produk','public');
      }
      $produk->save();

      return redirect('/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['produk'] = Produk::where('id','=',$id)->first();

      return view('produk.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $post = Produk::find($id);
      $produk->nama_produk = $request->nama_produk;
      $produk->deskripsi_produk = $request->deskripsi_produk;
      $produk->stock = $request->stock;
      $produk->harga = $request->harga;
      if ($request->hasFile('photo')) {
          $produk->photo =$request->photo->hashName();
          $request->photo->store('uploads/gambar_produk', 'public');
      }

      $produk->update();

      return redirect('produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $produk = Produk::where('id',$id);
      $produk->delete();
    }
}
