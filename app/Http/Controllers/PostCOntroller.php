<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use DataTables;
use App\Http\Requests\{UserUpdateRequest,UserAddRequest};
use App;

class PostController extends Controller
{


  function json()
  {
    return Datatables::of(Post::all())
    ->addColumn('action', function ($post) {
      return view('modal.post', [
      'model' => $post,
      'url_edit' => route('post.edit', $post->id),
      'url_destroy' => route('post.destroy', $post->id)
        ]);
    })
    ->rawColumns(['action'])
    ->make(true);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // $data['post'] = Post::all();
      //
      //
      // return view('post.index', $data);


      return view('post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $post = new Post;


      $post->title = $request->title;
      $post->body = $request->body;
      $post->save();

      return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['post'] = Post::where('id','=',$id)->first();

      return view('post.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $post = Post::find($id);
      $post->title = $request->title;
      $post->body = $request->body;
      $post->update();

      return redirect('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = Post::where('id',$id);
      $post->delete();

    }
}
